%
%   these simulations use the updated mechanical model with stronger
%   torsional components in SO and IO
%
close all
clear all
%addpath('Carlos/Saccade_Generators/')
%addpath('Carlos/Simulink_Models/')

%addpath('Carlos/Optimal_Control/')
%addpath('Carlos/System_Identification/')

addpath('ResultsPLoSCompBiol/')


saccade_amplitude_stddev = 15; % 15 degrees
number_saccades = 1500;

% generate list of saccades to do
[all_goals_origin, all_goals_sequence] = generate_gaussian_saccades(saccade_amplitude_stddev,number_saccades);

%% obtain models
sys = sysid();   % PBRS range 20 deg in experiments2, 15 deg in experiments3, 30 deg in experiments4, 10 deg in experimentst

sys_dz2 = sysid([0;0;0.02]);
sys_dz4 = sysid([0;0;0.04]);
sys_dz6 = sysid([0;0;0.06]);
 sys_dz8 = sysid([0;0;0.08]);
sys_dz_2 = sysid([0;0;-0.02]);
sys_dz_4 = sysid([0;0;-0.04]);
sys_dz_6 = sysid([0;0;-0.06]);
sys_dz_8 = sysid([0;0;-0.08]);
% 
sys_dy2 = sysid([0;0.02;0]);
sys_dy4 = sysid([0;0.04;0]);
sys_dy6 = sysid([0;0.06;0]);
sys_dy8 = sysid([0;0.08;0]);
sys_dy_2 = sysid([0;-0.02;0]);
sys_dy_4 = sysid([0;-0.04;0]);
sys_dy_6 = sysid([0;-0.06;0]);
sys_dy_8 = sysid([0;-0.08;0]);

% save all system models
save('sys.mat','sys','sys_dz2','sys_dz4','sys_dz6','sys_dz8','sys_dz_2','sys_dz_4','sys_dz_6','sys_dz_8','sys_dy2','sys_dy4','sys_dy6','sys_dy8','sys_dy_2','sys_dy_4','sys_dy_6','sys_dy_8');
% 
% obtain force approximations
Qf = force_approximation();      % note: now with the updated muscle insertions!

Qf_dz2 = force_approximation([0;0;0.02]);
Qf_dz4 = force_approximation([0;0;0.04]);
Qf_dz6 = force_approximation([0;0;0.06]);
Qf_dz8 = force_approximation([0;0;0.08]);
Qf_dz_2 = force_approximation([0;0;-0.02]);
Qf_dz_4 = force_approximation([0;0;-0.04]);
Qf_dz_6 = force_approximation([0;0;-0.06]);
Qf_dz_8 = force_approximation([0;0;-0.08]);
% 
Qf_dy2 = force_approximation([0;0.02;0]);
Qf_dy4 = force_approximation([0;0.04;0]);
Qf_dy6 = force_approximation([0;0.06;0]);
Qf_dy8 = force_approximation([0;0.08;0]);
Qf_dy_2 = force_approximation([0;-0.02;0]);
Qf_dy_4 = force_approximation([0;-0.04;0]);
Qf_dy_6 = force_approximation([0;-0.06;0]);
Qf_dy_8 = force_approximation([0;-0.08;0]);

% save all force approximations
save('Qf.mat','Qf','Qf_dz2','Qf_dz4','Qf_dz6','Qf_dz8','Qf_dz_2','Qf_dz_4','Qf_dz_6','Qf_dz_8','Qf_dy2','Qf_dy4','Qf_dy6','Qf_dy8','Qf_dy_2','Qf_dy_4','Qf_dy_6','Qf_dy_8');
% 

% run every simulation
load sys
load Qf

run_multiple_saccades(sys, all_goals_sequence, 0, 'aed', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/all_saccades_aed')
run_multiple_saccades(sys, all_goals_sequence, 0, 'ae', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/all_saccades_ae')
run_multiple_saccades(sys, all_goals_sequence, 0, 'ad', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/all_saccades_ad')
% 
run_multiple_saccades(sys, all_goals_sequence, 0, 'aed', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/all_saccades_none')
run_multiple_saccades(sys, all_goals_sequence, 0, 'aed', 'target','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/all_saccades_target')
run_multiple_saccades(sys, all_goals_sequence, 0, 'aed', 'trajectory','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/all_saccades_trajectory')

run_multiple_saccades(sys, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;0], Qf, 'ResultsPLoSCompBiol/all_saccades_dz0')
% 
run_multiple_saccades(sys_dz_2, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;-0.02], Qf_dz_2, 'ResultsPLoSCompBiol/all_saccades_dz_2')
run_multiple_saccades(sys_dz_4, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;-0.04], Qf_dz_4, 'ResultsPLoSCompBiol/all_saccades_dz_4')
run_multiple_saccades(sys_dz_6, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;-0.06], Qf_dz_6, 'ResultsPLoSCompBiol/all_saccades_dz_6')
run_multiple_saccades(sys_dz_8, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;-0.08], Qf_dz_8, 'ResultsPLoSCompBiol/all_saccades_dz_8')
run_multiple_saccades(sys_dz2, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;0.02], Qf_dz2, 'ResultsPLoSCompBiol/all_saccades_dz2')
run_multiple_saccades(sys_dz4, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;0.04], Qf_dz4, 'ResultsPLoSCompBiol/all_saccades_dz4')
run_multiple_saccades(sys_dz6, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;0.06], Qf_dz6, 'ResultsPLoSCompBiol/all_saccades_dz6')
run_multiple_saccades(sys_dz8, all_goals_sequence, 0, 'aed', 'none','asymmetrical', [0;0;0.08], Qf_dz8, 'ResultsPLoSCompBiol/all_saccades_dz8')
% 
% 
run_multiple_saccades( sys, generate_directional_saccades([0 1], 50, 12), 1, 'aed', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_aed')
run_multiple_saccades( sys, generate_directional_saccades([1 1], 50, 12), 1, 'aed', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_aed45')
run_multiple_saccades( sys, generate_directional_saccades([1 0], 50, 12), 1, 'aed', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_aed90')
%  run_multiple_saccades( sys, generate_directional_saccades([0 1], 50, 12), 1, 'tar', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_tar')
%
run_multiple_saccades( sys, generate_directional_saccades([1 1], 50, 12), 1, 'tar', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_tar45')
run_multiple_saccades( sys, generate_directional_saccades([1 0], 50, 12), 1, 'tar', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_tar90')
% 
run_multiple_saccades( sys, generate_directional_saccades([0 1], 50, 12), 1, 'aed', 'none','asymmetrical', [0;0;0], Qf, 'ResultsPLoSCompBiol/directional_dz0')
run_multiple_saccades( sys, generate_directional_saccades([1 1], 50, 12), 1, 'aed', 'none','asymmetrical', [0;0;0], Qf, 'ResultsPLoSCompBiol/directional_dz045')
run_multiple_saccades( sys, generate_directional_saccades([1 0], 50, 12), 1, 'aed', 'none','asymmetrical', [0;0;0], Qf, 'ResultsPLoSCompBiol/directional_dz090')
run_multiple_saccades( sys, generate_directional_saccades([0 1], 35, 8), 1, 'ae', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_ae')
run_multiple_saccades( sys, generate_directional_saccades([0 1], 35, 8), 1, 'ad', 'none','symmetrical', [0;0;0], [], 'ResultsPLoSCompBiol/directional_ad')

[Mvel_8_0, Evel_8_0] =run_single_saccade(sys,[0;0;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
[Mvel_8_5, Evel_8_5] =run_single_saccade(sys,[0;5;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
[Mvel_8_10, Evel_8_10] =run_single_saccade(sys,[0;10;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
[Mvel_8_15, Evel_8_15] =run_single_saccade(sys,[0;15;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
[Mvel_8_20, Evel_8_20] =run_single_saccade(sys,[0;20;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
[Mvel_8_25, Evel_8_25] =run_single_saccade(sys,[0;25;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
[Mvel_8_30, Evel_8_30] =run_single_saccade(sys,[0;30;8],[0;0;0],'ad','none','symmetrical',[0;0;0],[]);
