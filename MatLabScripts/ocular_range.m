clear all
close all
tic
addpath('Carlos/Simulink_Models/')

% important - switch simin sampling time to 0.5 s
% important - switch Q_imu and F sampling time to 0.1 sc

% IMPORTANT VARIABLES TO DEFINE
range = 35; %RANGE -> DEGREES THAT EACH MOTOR DEVIATES FROM REST (in model 1 it was 45...)
steps = 10;  %STEPS -> HOW MANY STEPS FROM REST TO MAX DEVIATION


delta_angle = 512*range/45/steps;
allT=[];
for i=-steps:steps
    for j=-steps:steps
        for l=-steps:steps
            if i == 0 && j==0 && l==0 
            
            else    
                allT=[allT;round(512+delta_angle*i) round(512+delta_angle*j) round(512+delta_angle*l)];
            end
        end
    end
end
allT_rand = allT(randperm(length(allT)),:);

simin = timeseries();
delta_t = 0.5; % 
for j=1:size(allT_rand,1)
    simin = addsample(simin,'Data',(allT_rand(j,:)-512)*45/512,'Time',(j-1)*delta_t);
end

app_zero = 1*10^-15; % very close to zero to avoid null quaternion when converting in simulator
y_0 = [app_zero;app_zero;app_zero];
warning('off','all')

sIn = Simulink.SimulationInput('Eve_v3_SimulatorJvO');
sIn = sIn.setVariable('simin',simin);
sIn = sIn.setVariable('y_0', y_0);
sIn = sIn.setVariable('d', [0;0;0]);
sIn = sIn.setModelParameter('StopTime',string(4630));

sIn = sIn.setBlockParameter('Eve_v3_SimulatorJvO/quaternion_output','SampleTime',string(0.01));   % 0.1??
sIn = sIn.setBlockParameter('Eve_v3_SimulatorJvO/simin','SampleTime',string(0.5));

simulation = sim(sIn);

q = simulation.Q_imu.Data(:,:);
r = q(:,2:4)./q(:,1);
degs = (360/pi)*atan(r);

% correlations= zeros(4630,1);

% 
% for j=1:4630
%     correlation = abs(corr(simulation.Velocity.Data((j-1)*50+1:(j-1)*50+50,2),simulation.Velocity.Data((j-1)*50+1:(j-1)*50+50,3)));
%     correlations(j) = correlation;
% end


figure (1); clf; 
scatter(degs(:,1),degs(:,2),2.5,'filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0])
nicegraph
title('Oculomotor range (xy plane)', 'fontsize', 20);
xlabel('r_x (deg)', 'fontsize', 18);
ylabel('r_y (deg)', 'fontsize', 18);
axis([-60 60 -60 60]);
xticks([-60 -40 -20 0.0 20 40 60]);
yticks([-60 -40 -20 0.0 20 40 60]);
set(gca, 'fontsize',16);

sx = 2*atan(std(r(:,1)))*180/pi    % in deg
sy = 2*atan(std(r(:,2)))*180/pi
sz = 2*atan(std(r(:,3)))*180/pi

mnrx=2*atan(min(r(:,1)))*180/pi
mxrx=2*atan(max(r(:,1)))*180/pi

mnry=2*atan(min(r(:,2)))*180/pi
mxry=2*atan(max(r(:,2)))*180/pi

mnrz=2*atan(min(r(:,3)))*180/pi
mxrz=2*atan(max(r(:,3)))*180/pi


figure (2);  clf;
scatter(degs(:,1),degs(:,3),2.5,'filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0])
hold on
nicegraph
title('Oculomotor range (xz plane)', 'fontsize', 20);
xlabel('r_x (deg)', 'fontsize', 18);
ylabel('r_z (deg)', 'fontsize', 18);
axis([-60 60 -60 60]);
xticks([-60 -40 -20 0.0 20 40 60]);
yticks([-60 -40 -20 0.0 20 40 60]);
set(gca, 'fontsize',16);set(gca, 'fontsize',16);
% s = sprintf('R_x=[%2.1f, %2.1f] deg', mnrx, mxrx);
% s2 = sprintf('s_x = %2.1f deg', sx);
% t = sprintf('R_y=[%2.1f, %2.1f] deg', mnry, mxry);
% t2 = sprintf('s_y = %2.1f deg', sy);
% v = sprintf('R_z=[%2.1f, %2.1f] deg', mnrz, mxrz);
% v2 = sprintf('s_z = %2.1f deg', sz);
% text(0.2, 0.125, s, 'fontsize',10);
% text(0.25, 0.075, s2, 'fontsize',10);
% text(0.2, 0.025, t, 'fontsize',10);
% text(0.25, -0.025, t2, 'fontsize',10);
% text(0.2, -0.075, v, 'fontsize',10);
% text(0.25, -0.125, v2, 'fontsize',10);

print -deps2c FigOculRngxz2

figure (3); clf;
scatter(degs(:,2),degs(:,3),2.5,'filled','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0])
nicegraph
title('Oculomotor range (yz plane)', 'fontsize', 20);
xlabel('r_y (deg)', 'fontsize', 18);
ylabel('r_z (deg)', 'fontsize', 18);
axis([-60 60 -60 60]);
xticks([-60 -40 -20 0.0 20 40 60]);
yticks([-60 -40 -20 0.0 20 40 60]);
set(gca, 'fontsize',16);
set(gca, 'fontsize',16);

print -deps2c FigOculRngyz2

toc
