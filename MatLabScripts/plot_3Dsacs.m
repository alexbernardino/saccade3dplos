function plot_3Dsacs(filename, select)

if nargin<2 
    select=0; 
end
if select
    load directional_tar;     % update, depending on the simulation type! 
    AMPSHOR = all_amplitudes;
    VPKHOR = all_peak_velocities;
    load directional_tar45;     % update, depending on the simulation type! 
    AMPS45 = all_amplitudes;
    VPK45 = all_peak_velocities;
     load directional_tar90;      % update, depending on the simulation type! 
    AMPS90 = all_amplitudes;
    VPK90 = all_peak_velocities;
end

load(filename)   % overwrite all the data
%
%  find the onsets and offsets of the saccades
%  Offsets:  'StopTime' was saccade_duration in seconds + 0.2 
% ( = 20 samples of 0.01 s)
%
all_r2 = [];   % will contain only the saccades between onset and offset
ON2 = all_durations/10 + 21;
Onsets = [1; cumsum(ON2)]; 
Onsets = Onsets(1:end-1);
Offsets = Onsets + all_durations/10+1;  % take one extra sample after saccade offset
begin_r = all_r(Onsets,:);
dvecs = final_r - begin_r; % difference vectors  (we will only look at y,z)
angles = zeros(length(all_amplitudes),1);
%
%  Calculate the correlations for the velocity profiles only
%  between onset and offset.
%
all_correlations2 = zeros(length(all_amplitudes),1);

for n=1:length(all_durations)
    vver = diff(all_r(Onsets(n):Offsets(n),2)); 
    vhor = diff(all_r(Onsets(n):Offsets(n),3));
    vcorr = abs(corrcoef(vhor,vver));
    all_correlations2(n) = vcorr(2,1);
    %
    %  store the rotation vectors only between on and offsets
    %
    all_r2 = [all_r2; all_r(Onsets(n):Offsets(n),:)];  
    %   calculate the saccade direction in deg: right = 0 down = 270 etc.
    %  
    angles(n) = (180/pi)*atan(dvecs(n,2)/(dvecs(n,3)+1e-10));
    if dvecs(n,3)>0 
        angles(n) = angles(n)+180; 
    end   
end

% Also only plot the rotation-vector data
%  from onset to offset!
%
% fit Listing's Plane through the data:    r_x = a + b.r_y + c.r_z
b		= regstats(all_r2(:,1),[all_r2(:,2) all_r2(:,3)],'linear',{'beta','r','rsquare','mse'});
% take away the bias on r_x:
rbx = all_r2(:,1) - b.beta(1);
% fit again
b		= regstats(rbx,[all_r2(:,2) all_r2(:,3)],'linear',{'beta','r','rsquare','mse'});
lpr = sqrt(1 + b.beta(2)^2 + b.beta(3)^2);
Prim = [1.0, -b.beta(2), -b.beta(3)]/lpr;  % the normalized primary direction 
sd_residuals = std(b.r);   % stdev around the plane: width of LP (this is not the same as stddev_rx)

%
%  calculate the rotation needed to bring all rotation vectors into
%  Linsting's coordinates: find the rotation axis that brings Prim to
%  [1,0,0]
%
P=[1,0,0];
[qprim, primprime, primaxis, primangle] = rotvecab(Prim, P)

%rprim = rotprim(Prim)    % the rotation to be applied to all rotation vectors
all_r_Listing = rotr(qprim, all_r);   % all rotation vectors are now in Listing coordinates

bL		= regstats(all_r_Listing(:,1),[all_r_Listing(:,2) all_r_Listing(:,3)],'linear',{'beta','r','rsquare','mse'});
lprL = sqrt(1 + bL.beta(2)^2 + bL.beta(3)^2);
PrimL = [1.0, bL.beta(2), bL.beta(3)]/lprL  % the normalized primary direction 
sd_residuals = std(bL.r)   % stdev around the plane: width of LP (this is not the same as stddev_rx)

figure(1); clf;
plot(all_r2(:,2), all_r2(:,3), 'k.');
hold on
axis([-.5,.5,-.5,.5]);
xticks([-0.4 -0.2 0 0.2 0.4]);
yticks([-0.4 -0.2 0 0.2 0.4]);
xlabel('r_y [rad/2]'); 
ylabel('r_z [rad/2]');
nicegraph

fname1 = strcat(filename,'Fig1yz');
k = sprintf('print -deps2c %s',fname1);
eval(k);

figure(2); clf;
plot(rbx, all_r2(:,3), 'k.');
hold on
axis([-.5,.5,-.5,.5]); 
plot([0 0],[-0.5 0.5],'k--','linewidth',1.5);
xticks([-0.4 -0.2 0 0.2 0.4]);
yticks([-0.4 -0.2 0 0.2 0.4]);
xlabel('r_x [rad/2]'); 
ylabel('r_z [rad/2]');
s_rx = 2.8*atan(stddev_rx)*180/pi;
s=sprintf('\\sigma_{rx} = %2.2f deg', s_rx);
t=sprintf('\\mu_{corr} = %2.2f', mean(all_correlations));
text(0.24, -0.4, s, 'fontsize', 12);
text(0.23, -0.45, t, 'fontsize', 12);

u =sprintf('n = [%2.3f, %2.3f, %2.3f]', Prim(1), Prim(2), Prim(3));
text(-0.48, 0.45, u, 'fontsize', 13); 
v = sprintf('Width LP: %2.3f rad/2', sd_residuals);
text(-0.48, 0.38, v, 'fontsize', 13);
v=sprintf('= (%2.2f deg)', (360/pi)*atan(sd_residuals));
text(-0.44, 0.31, v, 'fontsize',13);
nicegraph

fname1 = strcat(filename,'Fig2xz');
k = sprintf('print -deps2c %s',fname1);
eval(k);

x=all_amplitudes; 
y = all_peak_velocities;
fun = @(beta,x)(beta(1)*(1-exp(-beta(2)*x)));
b0 = [300, 0.4];
[beta, R, mse] = nlinfit(x,y,fun, b0);
xf = [0:1:65];
yf = beta(1)*(1-exp(-beta(2)*xf));

figure(3); clf;
plot(x, y, 'k.', 'markersize',8);
hold on
if ~select 
    plot(xf, yf, 'r--','linewidth', 1);
    s=sprintf('v_0 = %4.0f deg/s, \\alpha = %2.2f deg',beta(1),1/beta(2));
    text(2,750,s,'fontsize',14);
end

axis([0 65 0 800]);
xticks([0 10 20 30 40 50 60]);
yticks([0 200 400 600 800]);
xlabel('Saccade amplitude (deg)');
ylabel('Peak velocity (deg/s)');
hold on

if select
%    dir0 = find((angles<=3 | angles>357));  % horizontal saccade nrs starting near straight ahead
%    dir90 = find(angles<=93 & angles>87);  % vertical saccade nrs 
%    plot(x(dir0), y(dir0), 'ro', 'markerfacecolor','r');
%    plot(x(dir90), y(dir90), 'bo', 'markerfacecolor','b');
plot(AMPSHOR, VPKHOR, 'ko', 'markersize',8, 'markerfacecolor', 'r');  % 7 horizontal saccades starting from 0,0
x=AMPSHOR; 
y = VPKHOR;
fun = @(beta,x)(beta(1)*(1-exp(-beta(2)*x)));
b0 = [300, 0.4];
[beta, R, mse] = nlinfit(x,y,fun, b0);
xf = [0:1:65];
yf = beta(1)*(1-exp(-beta(2)*xf));
    plot(xf, yf, 'r--','linewidth', 1.5);
    s=sprintf('v_0 = %4.0f deg/s, \\alpha = %2.2f deg',beta(1),1/beta(2));
    text(2,750,s,'fontsize',14);
    
plot(AMPS90, VPK90, 'ko', 'markersize',8, 'markerfacecolor', 'b');  % 12 vertical saccades starting from 0,0
x=AMPS90; 
y = VPK90;
fun = @(beta,x)(beta(1)*(1-exp(-beta(2)*x)));
b0 = [300, 0.4];
[beta, R, mse] = nlinfit(x,y,fun, b0);
xf = [0:1:65];
yf = beta(1)*(1-exp(-beta(2)*xf));
    plot(xf, yf, 'b--','linewidth', 1.5);
    s=sprintf('v_0 = %4.0f deg/s, \\alpha = %2.2f deg',beta(1),1/beta(2));
    text(2,680,s,'fontsize',14);
    
plot(AMPS45, VPK45, 'ko', 'markersize',8, 'markerfacecolor', 'g');  % 12 vertical saccades starting from 0,0
x=AMPS45; 
y = VPK45;
fun = @(beta,x)(beta(1)*(1-exp(-beta(2)*x)));
b0 = [300, 0.4];
[beta, R, mse] = nlinfit(x,y,fun, b0);
xf = [0:1:65];
yf = beta(1)*(1-exp(-beta(2)*xf));
    plot(xf, yf, 'g--','linewidth', 1.5);
    s=sprintf('v_0 = %4.0f deg/s, \\alpha = %2.2f deg',beta(1),1/beta(2));
    text(2,710,s,'fontsize',14);
       
end
nicegraph

fname1 = strcat(filename,'Fig3RVpk');
k = sprintf('print -deps2c %s',fname1);
eval(k);

x=all_amplitudes; y = all_durations;
c = regstats(y,x,'linear',{'beta','r','rsquare'});
xf = [0:1:65];
yf = c.beta(1) + c.beta(2)*xf;

figure(4); clf;
plot(x, y, 'k.');
hold on
plot(xf, yf, 'r-','linewidth', 2);
axis([0 65 0 150]);
xticks([0 10 20 30 40 50 60]);
yticks([0 30 60 90 120 150]);
xlabel('Saccade amplitude (deg)');
ylabel('Duration (ms)');
hold on
nicegraph

figure(5); clf;
%
% only include oblique saccades: angle at least 20 deg away from the
% cardinal axes
%
select = ((angles>=20 & angles<=70) | (angles >= 110 & angles <=160) | (angles >= 200 & angles <= 250) | (angles >= -70 & angles <= -20));
histogram(all_correlations2(select), 50);
hold on;
m = mean(all_correlations2(select)); 
sc = std(all_correlations2(select));
xlabel('Correlation [h,v] velocity')
xticks([0 0.25 0.5 0.75 1]);
s=sprintf('\\mu = %2.2f  \\sigma = %2.2f', m, sc);
text(0.2, 60, s, 'fontsize',14);
nicegraph

fname1 = strcat(filename,'Fig4corr');
k = sprintf('print -deps2c %s',fname1);
eval(k);

% figure(8); clf;  % test the selection
% plot(cos(pi*angles/180), sin(pi*angles/180), 'k.');
% hold on
% plot(cos(pi*angles(select)/180), sin(pi*angles(select)/180), 'r.', 'markersize',8);
% 
figure(6); clf;
N=150;
plot(final_r(1:N,2), final_r(1:N,3), 'ro', 'markersize',10, 'markerfacecolor','r');
hold on
plot(begin_r(1:N,2), begin_r(1:N,3), 'k.', 'markersize',13);
plot([begin_r(1:N,2), final_r(1:N,2)],[begin_r(1:N,3) final_r(1:N,3)],'k-');
axis([-.6 .6 -.6 .6]);
xticks([-.6 -.4 -.2 0 .2 .4 .6]);
yticks([-.6 -.4 -.2 0 .2 .4 .6]);
xlabel('ry [rad/2]');
ylabel('rz [rad/2]');
plot(0.4,-0.5, 'k.','markersize',13);
plot(0.4,-0.55, 'ro', 'markersize',10, 'markerfacecolor','r');
text(0.45, -0.5, 'ONSET');
text(0.45, -0.55, 'OFFSET');
nicegraph

figure(7); clf;
plot(all_amplitudes, all_peak_velocities.*all_durations/1000, 'k.');
hold on
xlabel('saccade amplitude (deg)');
ylabel('v_{pk} x D (deg)');
axis([0 60 0 120]);
xticks([0 15 30 45 60]);
yticks([0 30 60 90 120]);
k		= regstats(all_durations.*all_peak_velocities/1000,all_amplitudes,'linear',{'beta','r','rsquare','mse'});
fitparameters = k.beta
fitsd = std(k.r)
plot([0 45], [k.beta(1) k.beta(1)+k.beta(2)*45],'r-','linewidth', 2);
nicegraph
s = sprintf('Slope: %2.2f', k.beta(2));
text(2, 110, s, 'fontsize',16);
s = sprintf('r^2: %2.2f', k.rsquare);
text(2, 100, s, 'fontsize',16);

print -deps2c combims
return

function r_L = rotr(qp, rr)   % rotate every rotation vector by rp
 
%
%  generate the rotation matrix from the primary quaternion
%
Rot = zeros(3,3);
Rot(1,1) = 1.0 - 2*qp(3)^2 - qp(4)^2;
Rot(2,2) = 1.0 - 2*qp(2)^2 - qp(4)^2;
Rot(3,3) = 1.0 - 2*qp(2)^2 - qp(3)^2;
Rot(1,2) = 2*qp(2)*qp(3) - 2*qp(1)*qp(4); 
Rot(1,3) = 2*qp(2)*qp(4) + 2*qp(1)*qp(3);
Rot(2,1) = 2*qp(2)*qp(3) + 2*qp(1)*qp(4);
Rot(2,3) = 2*qp(3)*qp(4) - 2*qp(1)*qp(2);
Rot(3,1) = 2*qp(2)*qp(4) - 2*qp(1)*qp(3);
Rot(3,2) = 2*qp(3)*qp(4) + 2*qp(1)*qp(2);

N = size(rr,1);
r_L = zeros(N,3);
for n=1:N
     r = rr(n,:)';
     r_L(n,:) = Rot * r;          % rotation 
end

return
