%% 3D simulator - debug and draw


% the Original initial positions taken by Carlos / Miguel's prototype
figure (11); clf;
Q1_0 = [-0.007; 0.05302; 0.05302];
Q2_0 = [-0.007; 0.05302; -0.05302];

Q3_0 = [-0.007; -0.07498; 0];
Q4_0 = [-0.007; 0.07498; 0];

Q5_0 = [-0.007; -0.05302; 0.05302];
Q6_0 = [-0.007; -0.05302; -0.05302];

theta = [0; 0; 0];
P1 = [-0.436 - 0.1*sin(theta(1)); -0.0835; -1*0.021 + 0.1*cos(theta(1))];
P2 = [-0.436 + 0.1*sin(theta(1)); -0.0835; -1*0.021 - 0.1*cos(theta(1))];

P3 = [-0.323 - 0.140*sin(theta(2)); -0.140*cos(theta(2)); -1*0.0475];
P4 = [-0.323 + 0.140*sin(theta(2)); 0.140*cos(theta(2)); -1*0.0475];

P5 = [-0.436 + 0.1*sin(theta(3)); 0.0835; -1*0.021 + 0.1*cos(theta(3))];
P6 = [-0.436 - 0.1*sin(theta(3)); 0.0835; -1*0.021 - 0.1*cos(theta(3))];

X1 = [-0.2055; 0; 0.0525];
X2 = [-0.2055; 0; -0.0525];

X5 = X1;%X5 = [-0.2055; 0; 0.0625];

X6 = X2;%X6 = [-0.2055; 0; -0.0625];

all_pointsQC = [Q1_0 Q2_0 Q3_0 Q4_0 Q5_0 Q6_0];
scatter3(all_pointsQC(1,:), all_pointsQC(2,:), all_pointsQC(3,:) ,'filled', 'MarkerFaceColor',[1 0 0]);

all_pointsPXC = [P1 P2 P3 P4 P5 P6 X1 X2 X5 X6];
AllPXC = [X1 X2 P3 P4 X5 X6];

hold on
scatter3(all_pointsPXC(1,:), all_pointsPXC(2,:), all_pointsPXC(3,:) ,'filled', 'MarkerFaceColor',[0 1 0]);
hold on
line([P1(1),X1(1)],[P1(2),X1(2)],[P1(3),X1(3)],'Color','black', 'LineWidth', 2)
hold on
line([P2(1),X2(1)],[P2(2),X2(2)],[P2(3),X2(3)],'Color','black', 'LineWidth', 2)
hold on
line([X1(1),Q1_0(1)],[X1(2),Q1_0(2)],[X1(3),Q1_0(3)],'Color','black', 'LineWidth', 2)
hold on
line([X2(1),Q2_0(1)],[X2(2),Q2_0(2)],[X2(3),Q2_0(3)],'Color','black', 'LineWidth', 2)
hold on
line([P3(1),Q3_0(1)],[P3(2),Q3_0(2)],[P3(3),Q3_0(3)],'Color','black', 'LineWidth', 2)
hold on
line([P4(1),Q4_0(1)],[P4(2),Q4_0(2)],[P4(3),Q4_0(3)],'Color','black', 'LineWidth', 2)
hold on
line([P5(1),X5(1)],[P5(2),X5(2)],[P5(3),X5(3)],'Color','black', 'LineWidth', 2)
hold on
line([P6(1),X6(1)],[P6(2),X6(2)],[P6(3),X6(3)],'Color','black', 'LineWidth', 2)
hold on
line([X5(1),Q5_0(1)],[X5(2),Q5_0(2)],[X5(3),Q5_0(3)],'Color','black', 'LineWidth', 2)
hold on
line([X6(1),Q6_0(1)],[X6(2),Q6_0(2)],[X6(3),Q6_0(3)],'Color','black', 'LineWidth', 2)
xlabel('x');
ylabel('y');
zlabel('z');

% hexagon
hexagon_Q = [Q1_0 Q5_0 Q3_0 Q6_0 Q2_0 Q4_0];
fill3(hexagon_Q(1,:),hexagon_Q(2,:),hexagon_Q(3,:),'magenta')

text(Q1_0(1)+0.005, Q1_0(2)+0.005, Q1_0(3)+0.005, 'SR', 'fontsize', 12);
text(Q2_0(1)+0.005, Q2_0(2)+0.005, Q2_0(3)+0.005, 'IR', 'fontsize', 12);
text(Q3_0(1)+0.005, Q3_0(2)+0.005, Q3_0(3)+0.005, 'MR', 'fontsize', 12);
text(Q4_0(1)+0.005, Q4_0(2)+0.005, Q4_0(3)+0.005, 'LR', 'fontsize', 12);
text(Q5_0(1)+0.005, Q5_0(2)+0.005, Q5_0(3)+0.005, 'SO', 'fontsize', 12);
text(Q6_0(1)+0.005, Q6_0(2)+0.005, Q6_0(3)+0.005, 'IO', 'fontsize', 12);

text(P1(1)+0.005, P1(2)+0.005, P1(3)+0.005, 'SR', 'fontsize', 12);
text(P2(1)+0.005, P2(2)+0.005, P2(3)+0.005, 'IR', 'fontsize', 12);
text(P3(1)+0.005, P3(2)+0.005, P3(3)+0.005, 'MR', 'fontsize', 12);
text(P4(1)+0.005, P4(2)+0.005, P4(3)+0.005, 'LR', 'fontsize', 12);
text(P5(1)+0.005, P5(2)+0.005, P5(3)+0.005, 'SO', 'fontsize', 12);
text(P6(1)+0.005, P6(2)+0.005, P6(3)+0.005, 'IO', 'fontsize', 12);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %    updated coordinates for the Pi and Xi positions
  %    with the aim to increase the torsional range
  %    and a bit more in line with the anatomy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure (12); clf;
% Q1_0 = [-0.007; 0.05302; 0.05302];
% Q2_0 = [-0.007; 0.05302; -0.05302];
Q1_0 = [-0.007; 0.005302; 0.05302];      % SR
Q2_0 = [-0.007; -0.005302; -0.05302];    % IR

Q3_0 = [-0.007; -0.07498; 0];                % LR
Q4_0 = [-0.007; 0.07498; 0];                 % MR

% Q5_0 = [-0.007; -0.05302; 0.05302];
% Q6_0 = [-0.007; -0.05302; -0.05302];
Q5_0 = [-0.007; -0.005302; 0.05302];     % SO
Q6_0 = [-0.007; 0.005302; -0.05302];     % IO

theta = [0; 0; 0];
P1 = [-0.436 - 0.1*sin(theta(1)); -0.0835; 1*0.021 - 0.1*cos(theta(1))];
P2 = [-0.436 + 0.1*sin(theta(1)); -0.0835; 1*0.021 + 0.1*cos(theta(1))];
% P1 = [-0.436 - 0.1*sin(theta(1)); -0.005; -1*0.021 + 0.1*cos(theta(1))];
% P2 = [-0.436 + 0.1*sin(theta(1)); -0.005; -1*0.021 - 0.1*cos(theta(1))];
 
% P3 = [-0.323 - 0.140*sin(theta(2)); -0.140*cos(theta(2)); -1*0.0475];
% P4 = [-0.323 + 0.140*sin(theta(2)); 0.140*cos(theta(2)); -1*0.0475];
P3 = [-0.323 - 0.140*sin(theta(2)); -0.140*cos(theta(2)); 0.005];
P4 = [-0.323 + 0.140*sin(theta(2)); 0.140*cos(theta(2)); 0.005];

P5 = [-0.436 + 0.1*sin(theta(3)); 0.0835; 1*0.021 + 0.1*cos(theta(3))];
P6 = [-0.436 - 0.1*sin(theta(3)); 0.0835; 1*0.021 - 0.1*cos(theta(3))];
% P5 = [-0.436 + 0.1*sin(theta(3)); 0.005; -1*0.021 + 0.1*cos(theta(3))];
% P6 = [-0.436 - 0.1*sin(theta(3)); 0.005; -1*0.021 - 0.1*cos(theta(3))];

X1 = [-0.2055; -0.075; -0.0525];   %  ring for the SR  => changed
X2 = [-0.2055; -0.075; +0.0525];  %  ring for the IR  => changed   
X3 = P3;
X4 = P4;
X5 = [-0.15; 0.15; 0.0625];     %  ring for the SO    => changed
X6 = [-0.15; 0.15; -0.0625];    %  ring for the IO    => changed

all_pointsQ = [Q1_0 Q2_0 Q3_0 Q4_0 Q5_0 Q6_0];
scatter3(all_pointsQ(1,:), all_pointsQ(2,:), all_pointsQ(3,:) ,'filled', 'MarkerFaceColor',[1 0 0]);
hold on
all_pointsPXJ = [P1 P2 P3 P4 P5 P6 X1 X2 X5 X6];
scatter3(all_pointsPXJ(1,:), all_pointsPXJ(2,:), all_pointsPXJ(3,:) ,'filled', 'MarkerFaceColor',[0 1 0]);
line([P1(1),X1(1)],[P1(2),X1(2)],[P1(3),X1(3)],'Color','black', 'LineWidth', 2)
line([P2(1),X2(1)],[P2(2),X2(2)],[P2(3),X2(3)],'Color','black', 'LineWidth', 2)
line([X1(1),Q1_0(1)],[X1(2),Q1_0(2)],[X1(3),Q1_0(3)],'Color','black', 'LineWidth', 2)
line([X2(1),Q2_0(1)],[X2(2),Q2_0(2)],[X2(3),Q2_0(3)],'Color','black', 'LineWidth', 2)
line([P3(1),Q3_0(1)],[P3(2),Q3_0(2)],[P3(3),Q3_0(3)],'Color','black', 'LineWidth', 2)
line([P4(1),Q4_0(1)],[P4(2),Q4_0(2)],[P4(3),Q4_0(3)],'Color','black', 'LineWidth', 2)
line([P5(1),X5(1)],[P5(2),X5(2)],[P5(3),X5(3)],'Color','black', 'LineWidth', 2)
line([P6(1),X6(1)],[P6(2),X6(2)],[P6(3),X6(3)],'Color','black', 'LineWidth', 2)
line([X5(1),Q5_0(1)],[X5(2),Q5_0(2)],[X5(3),Q5_0(3)],'Color','black', 'LineWidth', 2)
line([X6(1),Q6_0(1)],[X6(2),Q6_0(2)],[X6(3),Q6_0(3)],'Color','black', 'LineWidth', 2)

line([P1(1),P2(1)], [P1(2),P2(2)], [P1(3),P2(3)], 'Color',[0.6 0.6 0.6], 'linewidth', 6)
line([P3(1),P4(1)], [P3(2),P4(2)], [P3(3),P4(3)], 'Color',[0.6 0.6 0.6], 'linewidth', 6)
line([P5(1),P6(1)], [P5(2),P6(2)], [P5(3),P6(3)], 'Color',[0.6 0.6 0.6], 'linewidth', 6)

pmLMR = (P3+P4)/2;
pmSIR = (P1+P2)/2;
pmSIO = (P5+P6)/2;
line([pmLMR(1), pmLMR(1)],[pmLMR(2),pmLMR(2)],[pmLMR(3)-0.04, pmLMR(3)+0.04], 'Color', 'red','Linewidth',3);
line([pmSIR(1), pmSIR(1)],[pmSIR(2)-0.04,pmSIR(2)+0.04],[pmSIR(3), pmSIR(3)], 'Color', 'red','Linewidth',3);
line([pmSIO(1), pmSIO(1)],[pmSIO(2)-0.04,pmSIO(2)+0.04],[pmSIO(3), pmSIO(3)], 'Color', 'red','Linewidth',3);

xlabel('x','fontsize', 18);
ylabel('y','fontsize', 18);
zlabel('z','fontsize', 18);
plot3(-0.005,0,0,'k.', 'markersize',12);
plot3(X5(1), X5(2), X5(3), 'bo', 'markersize', 30, 'markerfacecolor',[0.8 0.8 0.8]);
plot3(X5(1), X5(2), X5(3), 'r.', 'markersize', 20);
text(X5(1),X5(2),X5(3)+0.015,'SO pulley','fontsize',12);
plot3(X6(1), X6(2), X6(3), 'bo', 'markersize', 30, 'markerfacecolor',[0.8 0.8 0.8]);
plot3(X6(1), X6(2), X6(3), 'r.', 'markersize', 20);
text(X6(1),X6(2),X6(3)+0.015,'IO pulley','fontsize',12);
plot3(X1(1), X1(2), X1(3), 'bo', 'markersize', 20, 'markerfacecolor',[0.8 0.8 0.8]);
plot3(X1(1), X1(2), X1(3), 'r.', 'markersize', 15);
plot3(X2(1), X2(2), X2(3), 'bo', 'markersize', 20, 'markerfacecolor',[0.8 0.8 0.8]);
plot3(X2(1), X2(2), X2(3), 'r.', 'markersize', 15);

text(-0.015, 0.005, 0.005, 'C', 'fontsize', 14, 'fontweight', 'bold');
xticks([-0.4 -0.3 -0.2 -0.1 0]);
yticks([-0.2 -0.1 0 0.1 0.2]);
nicegraph

% hexagon
hexagon_Q = [Q1_0 Q5_0 Q3_0 Q2_0 Q6_0 Q4_0];
fill3(hexagon_Q(1,:),hexagon_Q(2,:),hexagon_Q(3,:),[0.6 0.6 0.9])

% text(Q1_0(1)-0.01, Q1_0(2)+0.005, Q1_0(3)+0.005, 'SR', 'fontsize', 16);
% text(Q2_0(1)-0.01, Q2_0(2)+0.005, Q2_0(3)+0.005, 'IR', 'fontsize', 16);
% text(Q3_0(1)-0.01, Q3_0(2)+0.005, Q3_0(3)+0.005, 'MR', 'fontsize', 16);
% text(Q4_0(1)-0.01, Q4_0(2)+0.005, Q4_0(3)+0.005, 'LR', 'fontsize', 16);
% text(Q5_0(1)-0.01, Q5_0(2)+0.005, Q5_0(3)+0.005, 'SO', 'fontsize', 16);
% text(Q6_0(1)-0.01, Q6_0(2)+0.005, Q6_0(3)+0.005, 'IO', 'fontsize', 16);

text(P1(1)-0.01, P1(2)-0.015, P1(3)+0.005, 'SR', 'fontsize', 16);
text(P2(1)-0.01, P2(2)+0.015, P2(3)+0.005, 'IR', 'fontsize', 16);
text(P3(1)-0.01, P3(2)-0.015, P3(3)+0.005, 'LR', 'fontsize', 16);
text(P4(1)-0.01, P4(2)+0.015, P4(3)+0.005, 'MR', 'fontsize', 16);
text(P5(1)-0.01, P5(2)+0.02, P5(3)+0.005, 'SO', 'fontsize', 16);
text(P6(1)-0.01, P6(2)+0.025, P6(3)+0.005, 'IO', 'fontsize', 16);

%  Unit vectors
AllPXJ = [X1 X2 P3 P4 X5 X6];
TorqueJ = zeros(3,6);
TorqueJ2 = zeros(3,6);


for n=1:6
    TorqueC(:,n) = cross(all_pointsQ(:,n), (AllPXC(:,n)-all_pointsQ(:,n))/norm(AllPXC(:,n)-all_pointsQ(:,n)));
    TorqueC(:,n) = TorqueC(:,n)/norm(TorqueC(:,n));
end


% note that the direction of the force vector is from Q to X, so X - Q!
% also: invert the y-coordinate for Q, to get the correct direction for r!!

all_pointsQ(2,:) = -1*all_pointsQ(2,:);

for n=1:6
    TorqueJ2(:,n) = cross(all_pointsQ(:,n), (AllPXJ(:,n)-all_pointsQ(:,n))/norm(AllPXJ(:,n)-all_pointsQ(:,n)));
    TorqueJ2(:,n) = TorqueJ2(:,n)/norm(TorqueJ2(:,n));
end


%
%  calculate the total lengths of the muscles in m
%
Lengths = zeros(1,6);
    LSR = sqrt( (Q1_0(1)-X1(1))^2 + (Q1_0(2)-X1(2))^2 + (Q1_0(3)-X1(3))^2) + ...
          sqrt( (X1(1)-P1(1))^2 + (X1(2)-P1(2))^2 + (X1(3)-P1(3))^2);
    LIR = sqrt( (Q2_0(1)-X2(1))^2 + (Q2_0(2)-X2(2))^2 + (Q2_0(3)-X2(3))^2) + ...
          sqrt( (X2(1)-P2(1))^2 + (X2(2)-P2(2))^2 + (X2(3)-P2(3))^2);
    LLR = sqrt( (Q3_0(1)-X3(1))^2 + (Q3_0(2)-X3(2))^2 + (Q3_0(3)-X3(3))^2) + ...
          sqrt( (X3(1)-P3(1))^2 + (X3(2)-P3(2))^2 + (X3(3)-P3(3))^2);
    LMR = sqrt( (Q4_0(1)-X4(1))^2 + (Q4_0(2)-X4(2))^2 + (Q4_0(3)-X4(3))^2) + ...
          sqrt( (X4(1)-P4(1))^2 + (X4(2)-P4(2))^2 + (X4(3)-P4(3))^2);
    LSO = sqrt( (Q5_0(1)-X5(1))^2 + (Q5_0(2)-X5(2))^2 + (Q5_0(3)-X5(3))^2) + ...
          sqrt( (X5(1)-P5(1))^2 + (X5(2)-P5(2))^2 + (X5(3)-P5(3))^2);
    LIO = sqrt( (Q6_0(1)-X6(1))^2 + (Q6_0(2)-X6(2))^2 + (Q6_0(3)-X6(3))^2) + ...
          sqrt( (X6(1)-P6(1))^2 + (X6(2)-P6(2))^2 + (X6(3)-P6(3))^2);
    
 Lengths(1) = LSR; Lengths(2) = LIR; Lengths(3) = LLR; Lengths(4) = LMR;
 Lengths(5) = LSO; Lengths(6) = LIO;
 
%
% calculate the angles between the vectors of the moment arm and the force
% (the arccosine of the innerproduct between (OQ and (XQ) )
%
Angles = zeros(1,6);
ASR = dot(Q1_0, X1-Q1_0)/(norm(Q1_0)*norm(X1-Q1_0));
AIR = dot(Q2_0, X2-Q2_0)/(norm(Q2_0)*norm(X2-Q2_0));
ALR = dot(Q3_0, X3-Q3_0)/(norm(Q3_0)*norm(X3-Q3_0));
AMR = dot(Q4_0, X4-Q4_0)/(norm(Q4_0)*norm(X4-Q4_0));
ASO = dot(Q5_0, X5-Q5_0)/(norm(Q5_0)*norm(X5-Q5_0));
AIO = dot(Q6_0, X6-Q6_0)/(norm(Q6_0)*norm(X6-Q6_0));

Angles(1) = acos(ASR)*180/pi;
Angles(2) = acos(AIR)*180/pi;
Angles(3) = acos(ALR)*180/pi;
Angles(4) = acos(AMR)*180/pi;
Angles(5) = acos(ASO)*180/pi;
Angles(6) = acos(AIO)*180/pi;

%
%  sizes of the torque vectors 
%
LTorques = zeros(1,6);
for n=1:6
    TorqueJ3(:,n) = cross(all_pointsQ(:,n), (AllPXJ(:,n)-all_pointsQ(:,n))/norm(AllPXJ(:,n)-all_pointsQ(:,n)));
    LTorques(n) = norm(TorqueJ3(:,n));  % relative to the rest lengths
    LTorques(n) = LTorques(n)/Lengths(n);
end