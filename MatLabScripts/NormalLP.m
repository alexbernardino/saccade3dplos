LP = [
    -8 0.995 -0.088 0.055;
    -6 0.996 -0.081 0.032;
    -4 0.997 -0.080 0.007;
    -2 0.996 -0.083 -0.025;
    0  0.994 -0.087 -0.062;
    2  0.993 -0.040 -0.107;
    4  0.986 -0.055 -0.157;
    6  0.976 -0.070 -0.205;
    8  0.964 -0.085 -0.251;
    ];

angles = (180/pi)*atan(LP(:,4)./LP(:,2));

figure(1); clf;
plot(LP(:,1), angles, 'ko-', 'markersize', 10, 'markerfacecolor', 'r', 'linewidth',2);
xticks([-8 -4 0 4 8]);
xlabel('\Delta z (cm)');
ylabel('Pitch angle LP (deg)');
nicegraph
print -deps2c pitchLP