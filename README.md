# saccade3DPLOS #

Matlab Code used for the PLoSCompBiol paper.

### Contents ###

* SimulinkModel
Contains the files for eye plant simulator

* MatlabScripts
Contains the Matlab to execute the simulations, store results, and generate plots.

* ResultsPLoSCompBiol
Folder to store the results of the simulations in .mat files. Running all simulations take several hours. 

* FiguresPLoSCompBiol
Folder to store the generated figures for the paper. 


### Simulator ###

The eye model used is SimulinkModel/Eye_v3_Simulator.slx

It is used directly in the following scripts:
* run_multiple_saccades.m
* run_single_saccade.m
* sys_id.m
* force_approximation.m
* ocular_range.m

### Script run_all.m ###

The run_all.m script runs every simulation whose result are shown in the paper. 

It runs, by parts:

* generation of random gaussian saccades
* linear identification of state space models for systems both symmetrical and  
  asymmetrical force approximation through least squares (to be used in force 
  minimization approach) of the system both symmetrical and asymmetrical
* runs simulations for approaches used (aed, ae, ad, trajectory, target, force    
  minimization)
* runs directional saccades for velocity profile comparison (main sequence and 
  non-linear kinematic relations). 

This last part is not as automatic as the others, so a little tweaking with the variables can be useful for plotting

### Script saccade_planner.m ###

The saccade_planner.m script plans a specific saccade beginning in a certain orientation 'y_0' and ending in orientation 'goal'. 
Both are in half-radians. 
Depending on the inputs, the saccade is planned according to different approaches. 
This is probably the most crucial file, so I will detail a bit the inputs:

* sys: linear state space model of the system
* goal: end orientation in rotation vector notation
* y_0: starting orientation in rotation vector notation
* ene_dur_terms: classical optimization terms ('aed', 'ae' or 'ad')
* lp_constraints: direct penalizations from LP ('target', 'trajectory')
* symmetry: whether force minimization will be used (name is unfortunate). 
      'asymmetrical' means the force term will be present. 

In this case, you need to provide the next input. Should you prefer to update the name to something more intuitive, it is simply a question of replacing
* Qf: least squares approximation of quadratic form representing force (f ~ r^T Qf r)
* u_ini: initial positioning of the motors. 

Generally don't need to worry about this input: when running a single saccade, it does not make much difference; 
when running multiple in sequence, it fills itself with the last value of the last saccade

### Script run_single_saccade.m ###

The run_single_saccade.m script runs a single saccade from 'origin' to 'target', both in degrees. 
This file allows the plotting of trajectory and velocity profile (just uncomment the plot sections). 
The inputs are the same as the saccade_planner plus 'd', the offset in meters of the motors with respect to the eye (see my Thesis report for asymmetry).

### Script run_multiple_saccades.m ###

The run_multiple_saccades.m script runs multiple saccades, either starting every saccade from the origin ('origin'=1) or making them in sequence ('origin'=0). 
You also need to provide the list of targets, given in the all_goals variable. 
This kind of variable should be generated using a saccade generator file. 
You should also provide a string 'filename', the name of the file where the data will be saved for further analysis.
After collecting all the data, it finds the most probable orientation plane through least squares and stores: 

* all_r : all eye orientations
* all_amplitudes: list of amplitudes of all saccades done (for main sequence)
* all_durations: list of saccade durations (for main sequence)
* all_peak_velocities: list of peak velocities (for main sequence)
* all_correlations: list of correlations between horizontal and vertical velocity profiles for every saccade
* mean_rx: mean torsional component
* stddev_rx: standard deviation of torsional component
* mean_d: mean distance to the plane (in half radians)
* stddev_d: standard deviation of the distance to the plane (in half radians)
* final_r: final orientation
* n: normal to the plane found

### Script plot_3Dsaccades.m  ###

plots the results saved after running several saccades (run_multiple_saccades). 
            These are the characteristic rx ry, rx rz and ry rz plots shown in paper, 
      main-sequence plots and component stretching analysis. 

Regarding the crucial variables:

* stddev_d: standard deviation to the plane. 
  To obtain degrees, do 2*atan(stddev_d)*180/pi
* all_correlations: correlations between horizontal and vertical velocity profiles 
  (should be as close to 1 as possible). To obtain median, do mean(all_correlations)
* n: normal to the plane found. 

### Script sys_id.m  ###

Identifies the linear state space model which best resembles the non-linear simulator, sys, for a given offset 'd'.

### Script force_approximation.m  ###

Obtains the least squares approximation of quadratic form representing force, Qf, for a given offset 'd'.

### Script ocular_range.m  ###
Plots the ocular range of the model, as shown in the paper.

### Script draw_3d_model.m ###
Plots the eye model in 3d.




